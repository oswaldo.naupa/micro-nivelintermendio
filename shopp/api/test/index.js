const api = require('../dist');

const redis = {
    local: {
        host: "localhost",
        port: 6379,
        password: undefined
    },
    remote: {
        host: "localhost",
        port: 6379,
        password: undefined
    }
}

const environment = redis.local;

const users = [
    {
         fullName: 'Oswaldo Naupa',
         image: 'https://cdn.jwit.ar/public/jwit.webp',
         phone: "264545455",
         username: 'oswaldo23'  
    },
    {
         fullName: 'Oswaldo Naupa',
         image: 'https://cdn.jwit.ar/public/jwit.webp',
         phone: "264545455",
         username: 'oswaldo23'  
    }
]

async function create(user){

    try {
        
        const result = await api.Create(user, environment);

        console.log(result);

    } catch (error) {
        
        console.error( error );

    }

}

async function del( username ){

    /*const params = {
            ids: [3,67,56,8478],
            lotes: [ 454545454,5468745,78788  ],
            marks: [ 'etiqueta negra', 'don alejandro', ' la providencia', 'toro viejo']
        }*/

    try {
        
        const result = await api.Delete( { username }, environment);

        console.log(result);

    } catch (error) {
        
        console.error( error );

    }

}

async function update(params){

    try {
        
        const result = await api.Update(params, environment);

        console.log(result);

    } catch (error) {
        
        console.error( error );

    }

}

async function findOne(params){

    try {
        
        const result = await api.FindOne(params, environment);

        console.log(result);

    } catch (error) {
        
        console.error( error );

    }

}

async function view(params){

    try {
        
        const result = await api.View(params, environment);

        console.log(result);

    } catch (error) {
        
        console.error( error );

    }

}

const main = async () => {
    try {
        
        await view()

        //await create(users[0])

        //await view();

        await del(users[0].username)

        await view()

    } catch (error) {
        
        console.error(error);

    }
}

main();

