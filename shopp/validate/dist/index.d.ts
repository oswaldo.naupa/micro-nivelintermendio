import * as T from "./types";
export declare function create(params: T.Create.Request): Promise<T.Create.Response>;
export declare function del(params: T.Delete.Request): Promise<T.Delete.Response>;
export declare function view(params: T.View.Request): Promise<T.View.Response>;
//# sourceMappingURL=index.d.ts.map