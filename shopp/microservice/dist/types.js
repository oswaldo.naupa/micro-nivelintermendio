"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Adapters = exports.Models = void 0;
var Models;
(function (Models) {
    ;
    Models.attributes = ['id', 'user', 'product', 'createdAt', 'updateAt'];
    ;
    let Delete;
    (function (Delete) {
        ;
        ;
    })(Delete = Models.Delete || (Models.Delete = {}));
    let FindAndCountAll;
    (function (FindAndCountAll) {
        ;
        ;
    })(FindAndCountAll = Models.FindAndCountAll || (Models.FindAndCountAll = {}));
    let FindOne;
    (function (FindOne) {
        ;
        ;
    })(FindOne = Models.FindOne || (Models.FindOne = {}));
    let Update;
    (function (Update) {
        ;
        ;
    })(Update = Models.Update || (Models.Update = {}));
})(Models = exports.Models || (exports.Models = {}));
var Adapters;
(function (Adapters) {
    Adapters.endpoint = ['create', 'delete', 'view'];
})(Adapters = exports.Adapters || (exports.Adapters = {}));
//# sourceMappingURL=types.js.map