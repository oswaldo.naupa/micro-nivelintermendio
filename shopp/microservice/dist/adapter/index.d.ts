import { Job } from "bullmq";
import { Adapters, statusCode } from '../types';
export declare const Process: (job: Job<any, any, Adapters.Endpoint>) => Promise<{
    statusCode: statusCode;
    data: import("../types").Models.ModelAttributes;
    message: string;
} | {
    statusCode: statusCode;
    data: number;
    message: string;
} | {
    statusCode: statusCode;
    data: import("../types").Models.Paginate;
    message: string;
} | {
    statusCode: string;
    message: string;
    data?: undefined;
}>;
export declare const run: () => Promise<void>;
//# sourceMappingURL=index.d.ts.map