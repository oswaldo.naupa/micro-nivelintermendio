import * as T from "../types";
export declare function create(params: T.Service.Create.Request): Promise<T.Service.Create.Response>;
export declare function del(params: T.Service.Delete.Request): Promise<T.Service.Delete.Response>;
export declare function view(params: T.Service.View.Request): Promise<T.Service.View.Response>;
//# sourceMappingURL=index.d.ts.map