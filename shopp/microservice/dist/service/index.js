"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.view = exports.del = exports.create = void 0;
const Models = __importStar(require("../models"));
const settings_1 = require("../settings");
const Validation = __importStar(require("validate-shopp"));
const Controllers = __importStar(require("../controller"));
async function create(params) {
    try {
        await Validation.create(params);
        //validacion del usuario
        const user = (await Controllers.ValidateUser({ user: params.user })).data;
        if (!user.state) {
            throw { statusCode: 'validationError', message: 'El usuario no esta habilitado para realizar compras' };
        }
        //validacion del producto
        const product = (await Controllers.ValidateProduct({ product: params.product })).data;
        if (!product.state) {
            throw { statusCode: 'validationError', message: 'No hay stock del producto para realizar ventas' };
        }
        const { statusCode, data, message } = await Models.create(params);
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: 'Service Create', error: error.toString() });
        return { statusCode: 'error', message: settings_1.InternalError };
    }
}
exports.create = create;
;
async function del(params) {
    try {
        await Validation.del(params);
        let where = {};
        //solo plurales
        var optionals = ['id', 'product', 'user'];
        for (let x of optionals.map(v => v.concat('s')))
            if (params[x] !== undefined)
                where[x.slice(0, -1)] = params[x];
        const { statusCode, data, message } = await Models.del({ where });
        if (statusCode !== 'success')
            return { statusCode, message };
        return { statusCode: 'success', data };
    }
    catch (error) {
        console.error({ step: 'Service Delete', error: error.toString() });
        return { statusCode: 'error', message: settings_1.InternalError };
    }
}
exports.del = del;
;
async function view(params) {
    try {
        await Validation.view(params);
        let where = {};
        //solo plurales
        var optionals = ['user', 'product'];
        for (let x of optionals.map(v => v.concat('s')))
            if (params[x] !== undefined)
                where[x.slice(0, -1)] = params[x];
        const { statusCode, data, message } = await Models.findAndCountAll({ where });
        return { statusCode, data, message };
    }
    catch (error) {
        console.error({ step: 'Service View', error: error.toString() });
        return { statusCode: 'error', message: settings_1.InternalError };
    }
}
exports.view = view;
;
//# sourceMappingURL=index.js.map