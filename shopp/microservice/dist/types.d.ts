import * as S from "sequelize";
import { T as apiUser } from 'api-users';
import { T as apiProduct } from 'api-products';
export declare type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';
export declare namespace Models {
    interface ModelAttributes {
        id?: number;
        user?: number;
        product?: number;
        createdAt?: string;
        updateAt?: string;
    }
    const attributes: readonly ["id", "user", "product", "createdAt", "updateAt"];
    type attributes = typeof attributes[number];
    type Where = S.WhereOptions<ModelAttributes>;
    interface Model extends S.Model<ModelAttributes> {
    }
    interface Paginate {
        data: ModelAttributes[];
        itemCount: number;
        pageCount: number;
    }
    namespace SyncDB {
        interface Request extends S.SyncOptions {
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Count {
        interface Request extends Omit<S.CountOptions<ModelAttributes>, "group"> {
        }
        interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
    namespace Create {
        interface Request extends ModelAttributes {
        }
        interface Opts extends S.CreateOptions<ModelAttributes> {
        }
        interface Response {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        }
    }
    namespace Delete {
        interface Opts extends S.DestroyOptions<ModelAttributes> {
        }
        interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
    namespace FindAndCountAll {
        interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group"> {
        }
        interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace FindOne {
        interface Opts extends S.FindOptions<ModelAttributes> {
        }
        interface Response {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        }
    }
    namespace Update {
        interface Request extends ModelAttributes {
        }
        interface Opts extends S.UpdateOptions<ModelAttributes> {
        }
        interface Response {
            statusCode: statusCode;
            data?: [number, ModelAttributes[]];
            message?: string;
        }
    }
}
export declare namespace Service {
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            users?: number[];
            products?: number[];
        }
        interface Response {
            statusCode: statusCode;
            data?: Models.Paginate;
            message?: string;
        }
    }
    namespace Create {
        interface Request {
            user: number;
            product: number;
        }
        interface Response {
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            users?: number[];
            products?: number[];
        }
        interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
}
export declare namespace Controller {
    namespace ValidateUser {
        interface Request {
            user: number;
        }
        interface Response {
            statusCode: statusCode;
            data?: apiUser.Model;
            message?: string;
        }
    }
    namespace ValidateProduct {
        interface Request {
            product: number;
        }
        interface Response {
            statusCode: statusCode;
            data?: apiProduct.Model;
            message?: string;
        }
    }
    namespace Publish {
        interface Request {
            channel: string;
            instance: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: Request;
            message?: string;
        }
    }
}
export declare namespace Adapters {
    const endpoint: readonly ["create", "delete", "view"];
    type Endpoint = typeof endpoint[number];
    namespace BullConn {
        interface opts {
            concurrency: number;
            redis: Settings.REDIS;
        }
    }
}
export declare namespace Settings {
    interface REDIS {
        host: string;
        port: number;
        password: string;
    }
}
//# sourceMappingURL=types.d.ts.map