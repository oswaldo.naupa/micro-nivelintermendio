import { Sequelize } from 'sequelize';
import { createClient } from 'redis';
import { Adapters } from './types';
export declare const name: string;
export declare const version: number;
export declare const sequelize: Sequelize;
export declare const redisClient: ReturnType<typeof createClient>;
export declare const RedisOptionsQueue: Adapters.BullConn.opts;
export declare const Actions: {
    create: string;
    update: string;
    delete: string;
    orphan: string;
    error: string;
    start: string;
};
export declare const InternalError: string;
//# sourceMappingURL=settings.d.ts.map