import  { InternalError, redisClient, RedisOptionsQueue as opts} from "../settings";

import { Controller as T } from '../types';

import * as  apiUser from 'api-users';

import * as apiProduct from 'api-products';



export const Publish =  async (props: T.Publish.Request) : Promise<T.Publish.Response> => {

     try {
          
          if(!redisClient.isOpen) await redisClient.connect();

          await redisClient.publish(props.channel, props.instance);

          return { statusCode: "success", data: props };

     } catch (error) {
          
          console.error('error', { step: 'controller Publish', error });

          return { statusCode: 'error', message: InternalError };

     }
}

export const ValidateUser =  async (props: T.ValidateUser.Request) : Promise<T.ValidateUser.Response> => {

     try {
          
          const { statusCode, data, message } = await apiUser.FindOne({ id: props.user }, opts.redis);

          if( statusCode !== 'success') {
               
               switch(statusCode){

                    case 'notFound': throw { statusCode: 'validationError', message: 'No existe el usuario que intentas validar' }

                    default: throw { statusCode: 'error', message }
               }
          };

          return { statusCode: 'success', data }

     } catch (error) {
          
          console.error('error', { step: 'controller ValidateUser', error });

          return { statusCode: 'error', message: InternalError };

     }
}

export const ValidateProduct =  async (props: T.ValidateProduct.Request) : Promise<T.ValidateProduct.Response> => {

     try {
          
          const { statusCode, data, message } = await apiProduct.FindOne({ id: props.product }, opts.redis);

          if( statusCode !== 'success') {
               
               switch(statusCode){

                    case 'notFound': throw { statusCode: 'validationError', message: 'No existe el producto que intentas validar' }

                    default: throw { statusCode: 'error', message }
               }
          };

          return { statusCode: 'success', data }

     } catch (error) {
          
          console.error('error', { step: 'controller ValidateProduct', error });

          return { statusCode: 'error', message: InternalError };

     }
}