export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export namespace User {

    export interface Model {
        id: number,
        username: string,
        password: string,
        fullName: string,
        image: string,
        phone: number,
        state: boolean,
        createdAt: string,
        updateAt: string
    };

    export interface Paginate {
        data: Model[],
        itemCount: number,
        pageCount: number
    }

    export namespace FindOne {
        export interface Request {
            username?: string,
            id?: number,
        }
        export interface Response {
            statusCode: statusCode,
            data?: Model,
            message?: string
        }
    }

    export namespace View {

        export interface Request {
            offset?: number,
            limit?: number,

            state?: boolean
        }

        export interface Response {
            statusCode: statusCode,
            data?: Paginate,
            message?: string
        }
    }

    export namespace Update {

        export interface Request {
            username: string,
            fullName?: string,
            phone?: number,
            image?: string,
        }
        export interface Response {
            statusCode: statusCode,
            data?: Model,
            message?: string
        }
    }

    export namespace Create {
        export interface Request {
            username: string,
            fullName: string,
            phone: number,
            image: string,
        }
        export interface Response {
            statusCode: statusCode,
            data?: Model,
            message?: string
        }
    }
    export namespace Delete {
        export interface Request {
            username: string
        }
        export interface Response {
            statusCode: statusCode,
            data?:   Model,
            message?: string
        }
    }

}

export namespace Product {

    export interface Model {
        id: number,
        name: string,
        mark: string,
        year: string,
        store: string,
        image: string,
        lote: number,
        state: boolean,
        createdAt: string,
        updateAt: string
    };

    export interface Paginate {
        data: Model[],
        itemCount: number,
        pageCount: number
    }

    export namespace FindOne {
        export interface Request {
            id: number,
        }
        export interface Response {
        statusCode: statusCode,
        data?: Model,
        message?: string
        }
    }

    export namespace View {

        export interface Request {
            offset?: number,
            limit?: number,

            year?: string,
            state?: boolean

            marks?: string[],
            stores?: string[],
            lotes?: number[],
        }

        export interface Response {
        statusCode: statusCode,
        data?: Paginate,
        message?: string
        }
    }

    export namespace Update {

        export interface Request {
            id?: number,
            name?: string,
            mark?: string,
            year?: string,
            store?: string,
            image?: string,
            lote?: number,
            state?: boolean,
        }
        export interface Response {
        statusCode: statusCode,
        data?: Model,
        message?: string
        }
    }

    export namespace Create {
        export interface Request {
            name: string,
            mark: string,
            year?: string,
            store?: string,
            image?: string,
            lote?: number,
        }
        export interface Response {
        statusCode: statusCode,
        data?: Model,
        message?: string
        }
    }

    export namespace Delete {
        export interface Request {
            ids?: number[],
            marks?: string[],
            years?: string[],
            stores?: string[],
            lotes?: number[],
            state?: boolean,
        }
        export interface Response {
        statusCode: statusCode,
        data?:  number,
        message?: string
        }
    }
}

export namespace Shopp {

    export interface Model {
        id: number,
        user: number,
        product: number,
        createdAt: string,
        updateAt: string
    };
    
    export interface Paginate {
        data: Model[],
        itemCount: number,
        pageCount: number
    }
    
    export namespace View {
    
        export interface Request {
            offset?: number,
            limit?: number,
    
            users?: number[],
            products?: number[],
        }
    
        export interface Response {
           statusCode: statusCode,
           data?: Model,
           message?: string
        }
    }
    
    export namespace Create {
        export interface Request {
            user: number,
            product: number,
        }
        export interface Response {
           statusCode: statusCode,
           data?: Model,
           message?: string
        }
    }
    
    export namespace Delete {
        export interface Request {
            ids?: number[],
    
            users?: number[],
            products?: number[],
        }
        export interface Response {
           statusCode: statusCode,
           data?:  number,
           message?: string
        }
    }
}