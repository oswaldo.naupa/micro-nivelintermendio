interface redis {
    host: string;
    port: number;
    password: string;
}
export declare const redis: redis;
export declare const InternalError: string;
export {};
//# sourceMappingURL=settings.d.ts.map