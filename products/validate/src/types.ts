export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError';

export namespace FindOne {
    export interface Request {
        id: string,
    }
    export interface Response {
       statusCode: statusCode,
       data?: Request,
       message?: string
    }
}

export namespace View {

    export interface Request {
        offset?: number,
        limit?: number,

        year?: string,
        state?: boolean

        marks?: string[],
        stores?: string[],
        lotes?: number[],
    }

    export interface Response {
       statusCode: statusCode,
       data?: Request,
       message?: string
    }
}

export namespace Update {

    export interface Request {
        id?: number,
        name?: string,
        mark?: string,
        year?: string,
        store?: string,
        image?: string,
        lote?: number,
        state?: boolean,
    }
    export interface Response {
       statusCode: statusCode,
       data?: Request,
       message?: string
    }
}

export namespace Create {
    export interface Request {
        name: string,
        mark: string,
        year?: string,
        store?: string,
        image?: string,
        lote?: number,
    }
    export interface Response {
       statusCode: statusCode,
       data?: Request,
       message?: string
    }
}

export namespace Delete {
    export interface Request {
        ids?: number[],
        marks?: string[],
        years?: string[],
        stores?: string[],
        lotes?: number[],
        state?: boolean,
    }
    export interface Response {
       statusCode: statusCode,
       data?: Request,
       message?: string
    }
}