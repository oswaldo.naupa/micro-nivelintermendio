import Joi from 'joi';
import * as T from "./types";

export async function create(params: T.Create.Request) : Promise<T.Create.Response>{

    try {

        const scheme = Joi.object({
            name: Joi.string().required(),
            mark: Joi.string().required(),
            year: Joi.date(),
            image: Joi.string().uri(),
            store:  Joi.string(),
            lote: Joi.number(),
        })

        const result = await scheme.validateAsync(params);

        return { statusCode: 'success', data: params }

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    }
};

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {

    try {

        const scheme = Joi.object({
            ids: Joi.array().items(Joi.number().required()),
            marks:  Joi.array().items(Joi.string().required()),
            years:   Joi.array().items(Joi.string().required()),
            stores:   Joi.array().items(Joi.string().required()),
            lotes:   Joi.array().items(Joi.number().required()),
            state: Joi.boolean(),
        }).or('ids', 'marks', 'years', 'stores', 'lotes', 'state')

        const result = await scheme.validateAsync(params);

        return { statusCode: 'success', data: params }

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    }
};

export async function update(params: T.Update.Request): Promise<T.Update.Response> {

    try {

        const scheme = Joi.object({
            id: Joi.number().required(),
            name: Joi.string(),
            mark: Joi.string(),
            year: Joi.string(),
            store: Joi.string(),
            image: Joi.string().uri(),
            lote: Joi.number(),
            state: Joi.boolean(),
        })

        const result = await scheme.validateAsync(params);

        return { statusCode: 'success', data: params }

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    }
};

export async function view(params: T.View.Request): Promise<T.View.Response> {

    try {

        const scheme = Joi.object({
            offset: Joi.number(),
            limit: Joi.number(),
            year:   Joi.string(),
            state: Joi.boolean(),
            marks:  Joi.array().items(Joi.string().required()),
            stores:   Joi.array().items(Joi.string().required()),
            lotes:   Joi.array().items(Joi.number().required()),
        })

        const result = await scheme.validateAsync(params);

        return { statusCode: 'success', data: params }

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    }
};

export async function findOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {

    try {

        const scheme = Joi.object({
            id: Joi.number().required(),
        })

        const result = await scheme.validateAsync(params);

        return { statusCode: 'success', data: params }

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    }
};
